//
//  ContentView.swift
//  BMI
//
//  Created by Ratchanan Houyyai on 3/11/20.
//  Copyright © 2020 Chaichana Sudjairak. All rights reserved.
//

import SwiftUI

class bmiSetting: ObservableObject {
    @Published var bmi:Double = 0.0
    
}

struct ContentView: View {
    @ObservedObject var settings = bmiSetting()
    @State private var height: String = ""
    @State private var weight: String = ""
    @State private var showOverfat = false
    @State private var showFat = false
    @State private var showOvermin = false
    @State private var showMin = false
    @State private var showLessmin = false
    func bmiCalculate() {
        if self.height == "" || self.weight == "" {
            self.settings.bmi = 0.0
        }else {
            let yourHeight = Double(self.height)
            let yourWeight = Double(self.weight)
            if yourHeight == 0.0 && yourWeight == 0.0 || yourHeight == 0 {
                self.settings.bmi = 0.1
            }else {
                self.settings.bmi =  yourWeight! / ((yourHeight! / 100) * (yourHeight! / 100))
            }
        }
    }
    func bmiClear() {
        self.settings.bmi = 0.0
        self.height = ""
        self.weight = ""
    }
    var body: some View {
        TabView {
            VStack {
                Spacer()
                    .frame(height: 30)
                Text("คำนวณค่าดัชนีมวลกาย").font(.largeTitle).fontWeight(.semibold)
                Divider()
                Spacer()
                    .frame(height: 30)
                VStack {
                    HStack {
                        Text("ความสูง: ")
                        TextField("เซนติเมตร", text: $height).keyboardType(.numberPad)
                            
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                    }.padding(.horizontal)
                    HStack {
                        Text("น้ำหนัก:  ")
                        TextField("กิโลกรัม", text: $weight).keyboardType(.numberPad).textFieldStyle(RoundedBorderTextFieldStyle())
                    }.padding(.horizontal)
                    Spacer()
                        .frame(height: 20)
                    HStack {
                        Group {
                            Button(action: {
                                self.bmiCalculate()
                            }) {
                                
                                Text("คำนวณ")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color.white)                    .padding()
                                    .background(Color.blue)
                                    .cornerRadius(5)
                            }
                        }
                        Group {
                            Button(action: {
                                self.bmiClear()
                            }) {
                                
                                Text("    ล้าง    ")
                                    .font(.system(size: 20))
                                    .foregroundColor(Color.black)                    .padding()
                                    .background(Color.yellow)
                                    .cornerRadius(5)
                            }
                        }
                    }
                    Divider()
                }
                VStack {
                    Spacer()
                        .frame(height: 50)
                    Text("ค่าที่ได้คือ \(settings.bmi)")
                        .font(.system(size: 25))
                    Spacer()
                        .frame(height: 50)
                    if settings.bmi > 199.9 {
                        Text("Impossible!")
                            .font(.system(size: 45))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                            .frame(width: 380.0, height: 150.0)
                            .background(Color.red)
                    }
                    else if settings.bmi > 99.9 {
                        Text("คุณอ้วนมากๆๆๆไปแล้วนะ!")
                            .font(.system(size: 35))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                            .frame(width: 380.0, height: 150.0)
                            .background(Color.red)
                    }
                    else if settings.bmi > 49.9 {
                        Text("คุณอ้วนมากๆไปแล้วนะ!")
                            .font(.system(size: 35))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                            .frame(width: 380.0, height: 150.0)
                            .background(Color.red)
                    }
                    else if settings.bmi > 29.9 {
                        Text("คุณอ้วนมากไปแล้วนะ!")
                            .font(.system(size: 35))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                            .frame(width: 380.0, height: 150.0)
                            .background(Color.red)
                    }
                    else if settings.bmi > 24.9 {
                        Text("คุณอ้วนแล้วล่ะ")
                            .font(.system(size: 40))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                            .frame(width: 380.0, height: 150.0)
                            .background(Color.orange)
                    }
                    else if settings.bmi > 22.9 {
                        Text("คุณน้ำหนักเกินมาตรฐานแล้ว")
                            .font(.system(size: 30))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                            .frame(width: 380.0, height: 150.0)
                            .background(Color.yellow)
                    }
                    else if settings.bmi > 18.5 {
                        Text("คุณมีน้ำหนักปกติ")
                            .font(.system(size: 40))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                            .frame(width: 380.0, height: 150.0)
                            .background(Color.green)
                    }
                    else if settings.bmi > 1 {
                        Text("คุณผอมเกินไป")
                            .font(.system(size: 40))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                            .frame(width: 380.0, height: 150.0)
                            .background(Color.pink)
                    }
                    else if settings.bmi > 0 {
                        Text("นี่มันไม่น่าใช้น้ำหนักคน?\n ตรวจสอบด้วยนะ")
                            .font(.system(size: 30))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                            .frame(width: 380.0, height: 150.0)
                            .background(Color.red)
                    }
                    else if settings.bmi < -0.99 {
                        Text("น้ำหนักติดลบได้ด้วยเหรอ?")
                            .font(.system(size: 30))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                            .frame(width: 380.0, height: 150.0)
                            .background(Color.red)
                    }
                    else {
                        Text("มาลองคำนวณกันเถอะ")
                            .font(.system(size: 25))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                            .frame(width: 380.0, height: 150.0)
                            .background(Color.blue)
                    }
                }
            }
                
            .tabItem {
                Image(systemName: "1.circle")
                Text("คำนวณ")
            }.tag(0)
            VStack() {
                Spacer()
                .frame(height: 30)
                Text("เรื่องควรรู้")
                    .font(.title)
                Divider()
                ScrollView {
                    VStack(alignment: .leading) {
                        Group {
                            Text("อ้วนมาก (30.0 ขึ้นไป)").font(.headline)
                            Text("ค่อนข้างอันตราย เพราะเข้าเกณฑ์อ้วนมาก\nเสี่ยงต่อการเกิดโรคร้ายแรงที่แฝงมากับความอ้วน\nหากค่า BMI อยู่ในระดับนี้\nจะต้องระวังการรับประทานไขมัน\n และควรออกกำลังกายอย่างสม่ำเสมอ \nและหากเลขยิ่งสูงกว่า 40.0\nยิ่งแสดงถึงความอ้วนที่มากขึ้น")
                        }
                        Divider()
                        Group {
                            Text("อ้วน (25.0 - 29.9)").font(.headline)
                            Text("คุณอ้วนในระดับหนึ่ง ถึงแม้จะไม่ถึง\nเกณฑ์ที่ถือว่าอ้วนมาก ๆ แต่ก็ยังมีความเสี่ยงต่อ\nการเกิดโรคที่มากับความอ้วนได้เช่นกัน ทั้งโรคเบาหวาน\nและความดันโลหิตสูง")
                        }
                        Divider()
                        Group {
                            Text("น้ำหนักเกิน (23.0 - 24.9)").font(.headline)
                            Text("พยายามอีกนิดเพื่อลดน้ำหนักให้เข้าสู่ค่ามาตรฐาน\nเพราะค่า BMI ในช่วงนี้ยังถือว่าเป็น\nกลุ่มผู้ที่มีความอ้วนอยู่บ้าง แม้จะไม่ถือว่าอ้วน\nแต่หากประวัติคนในครอบครัวเคยเป็นโรคเบาหวาน\nและความดันโลหิตสูง \nก็ถือว่ายังมีความเสี่ยงมากกว่าคนปกติ")
                        }
                        Divider()
                        Group {
                            Text("น้ำหนักปกติ เหมาะสม (18.6 - 22.9)").font(.headline)
                            Text("น้ำหนักที่เหมาะสมสำหรับคนไทยคือค่า BMI \nระหว่าง 18.5-22.9 จัดอยู่ในเกณฑ์ปกติ\nห่างไกลโรคที่เกิดจากความอ้วน และมีความเสี่ยง\nต่อการเกิดโรคต่าง ๆ น้อยที่สุด \nควรพยายามรักษาระดับค่า BMI ให้อยู่ในระดับนี้ให้นานที่สุด")
                        }
                        Divider()
                        Group {
                            Text("ผอมเกินไป (น้อยกว่า 18.5)").font(.headline)
                            Text("น้ำหนักน้อยกว่าปกติก็ไม่ค่อยดี หากคุณสูงมาก\nแต่น้ำหนักน้อยเกินไป อาจเสี่ยงต่อการ\nได้รับสารอาหารไม่เพียงพอหรือได้รับพลังงานไม่เพียงพอ ส่งผลให้ร่างกายอ่อนเพลียง่าย")
                        }
                    }
                    .padding(5.0)
                    
                }
                
                
            }
            .tabItem {
                Image(systemName: "2.circle")
                Text("รายละเอียด")
            }.tag(1)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

